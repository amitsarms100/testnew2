import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Test2 } from './test2';

@NgModule({
  declarations: [
    Test2,
  ],
  imports: [
    IonicModule.forChild(Test2),
  ],
  exports: [
    Test2
  ]
})
export class Test2Module {}
